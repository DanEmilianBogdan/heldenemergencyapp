package com.example.lenovo.heldenemergencyapp;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IpService {
    @GET("/")
    Call<IP> getIP();
}

package com.example.lenovo.heldenemergencyapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    Button guideButton;
    SignInButton signInButton;
    private LoginGoogle loginGoogle;
    public static final int REQ_CODE = 600;
    public static final String PREFERENCE = "preference";
    public static final String PREF_NAME = "name";
    public static final String PREF_URL = "url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginGoogle = new LoginGoogle();
        signInButton = findViewById(R.id.bn_login);
        guideButton=findViewById(R.id.bn_guide);
        signInButton.setOnClickListener(this);
        guideButton.setOnClickListener(this);


        Log.d("CHECKING" , "ON CREATE");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bn_login:
                if(isInternetActive()) {
                    loginGoogle.login(LoginActivity.this);
                }
                else
                Toast.makeText(getApplicationContext(),"Please connect to internet", Toast.LENGTH_LONG).show();

                break;
            case R.id.bn_guide:
                startActivity(new Intent(getApplicationContext(), EmergencyGuideActivity.class));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("CHECKING" , "ACTIVITY RESULT GOOGLE");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            getResult(result);
        } else {


        }
    }

    public void getResult(GoogleSignInResult result) {
        Log.d("CHECKING" , "GET RESULT");
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            SharedPreferences mSharedPreference = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = mSharedPreference.edit();
            mEditor.putString(PREF_NAME, account.getDisplayName());


            try{
                mEditor.putString(PREF_URL, account.getPhotoUrl().toString());
            }catch (NullPointerException e){
                e.printStackTrace();
            }
            startActivity(new Intent(this,MainActivity.class));
//            session.setLoggedin(true);
            mEditor.apply();
            finish();

        } else {
            Toast.makeText(getApplicationContext(), "Google Login Failed", Toast.LENGTH_LONG).show();
            finish();
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
    }

    private boolean isInternetActive()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).
                getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).
                        getState() == NetworkInfo.State.CONNECTED;
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
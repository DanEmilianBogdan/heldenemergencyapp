package com.example.lenovo.heldenemergencyapp;

public class EventData {


    private String eventName, eventDescription;
    private long eventId;


//    public EventData(long eventId, String eventName, String eventDescription)
//    {
//        this.setEventId(eventId);
//        this.setEventName(eventName);
//        this.setEventDescription(eventDescription);
//    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

}

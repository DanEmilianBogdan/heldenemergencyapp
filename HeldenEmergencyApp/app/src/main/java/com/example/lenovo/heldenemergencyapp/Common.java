package com.example.lenovo.heldenemergencyapp;

public class Common
{
    private static final String BASE_URL ="http://ip.jsontest.com/";

    public static IpService getIpService()
    {
        return RetrofitClient.getCLIENT(BASE_URL).create(IpService.class);
    }
}

package com.example.lenovo.heldenemergencyapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class EventListAdapter extends BaseAdapter {

    Context context;

    public EventListAdapter(Context context, ArrayList<EventData> events) {
        this.context = context;
        this.events = events;
    }

    ArrayList<EventData> events;

    @Override
    public int getCount() {
        return events.size();
    }

    @Override
    public Object getItem(int position) {
        return events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.event_list_item, parent,false);
        }
        TextView id=convertView.findViewById(R.id.event_id);
        TextView name=convertView.findViewById(R.id.event_name);
        TextView description=convertView.findViewById(R.id.event_description);

        EventData eventData= (EventData) this.getItem(position);

        id.setText(String.valueOf(eventData.getEventId()));
        name.setText(eventData.getEventName());
        description.setText(eventData.getEventDescription());


        return convertView;
    }
}

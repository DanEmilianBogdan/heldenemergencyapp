package com.example.lenovo.heldenemergencyapp;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EmergencyGuideActivity extends AppCompatActivity {

    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;
    ImageView iv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_guide);

        ExpandableListView listView = findViewById(R.id.lvExp);
        initData();
        ExpandableListAdapter listAdapter = new ExpandableListAdapter(this, listDataHeader, listHash);
        listView.setAdapter(listAdapter);
    }

    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Earthquakes");
        listDataHeader.add("Floods");
        listDataHeader.add("Hurricanes");

        List<String> earthquakes = new ArrayList<>();
        //!!!!!!Citire din fisier;
        earthquakes.add("Prepare BEFORE\n" +
                "- Secure items, such as televisions, and objects that hang on walls. Store heavy and breakable objects on low shelves.\n" +
                "- Create a family emergency communications plan that has an out-of-state contact. Plan where to meet if you get separated.\n" +
                " - Make a supply kit that includes enough food and water for at least three days, a flashlight, a fire extinguisher, and a whistle. Consider each person’s specific needs, including medication. Do not forget the needs of pets. Have extra batteries and charging devices for phones and other critical equipment.\n" +
                "\n" +
                "Survive DURING\n" +
                "- Drop to your hands and knees. Cover your head and neck with your arms. Hold on to any sturdy furniture until the shaking stops. Crawl only if you can reach better cover without going through an area with more debris.\n" +
                "- If inside, stay there until the shaking stops. DO NOT run outside. DO NOT use elevators.\n" +
                "- If in a vehicle, stop in a clear area that is away from buildings, trees, overpasses, underpasses, or utility wires.\n" +
                "\n" +
                "Be Safe AFTER\n" +
                "- Expect aftershocks to follow the largest shock of an earthquake.\n" +
                "- If in a damaged building, go outside and quickly move away from the building.\n" +
                "- Do not enter damaged buildings.\n" +
                "- If you are trapped, cover your mouth. Send a text, bang on a pipe or wall, or use a whistle instead of shouting so that rescuers can locate you.\n");

        List<String> floods = new ArrayList<>();
        floods.add("Prepare BEFORE\n" +
                "- Learn and practice evacuation routes, shelter plans, and flash flood response.\n" +
                "- Gather supplies in case you have to leave immediately, or if services are cut off. Keep in mind each person’s specific needs, including medication. Don’t forget the needs of pets. Obtain extra batteries and charging devices for phones and other critical equipment.\n" +
                "- Keep important documents in a waterproof container. Create password-protected digital copies.\n" +
                "- Protect your property. Move valuables to higher levels. Declutter drains and gutters. Install check valves. Consider a sump pump with a battery.\n" +
                "\n" +
                "Survive DURING\n" +
                "- If told to evacuate, do so immediately. Do not walk, swim, or drive through flood waters. Turn Around.\n" +
                "- Stay off bridges over fast-moving water. Fast-moving water can wash bridges away without warning.\n" +
                "- If your vehicle is trapped in rapidly moving water, then stay inside. If water is rising inside the vehicle, then seek refuge on the roof.\n" +
                "- If trapped in a building, then go to its highest level. Do not climb into a closed attic. You may become trapped by rising floodwater. Go on the roof only if necessary. Once there, signal for help.\n" +
                "\n" +
                "Be Safe AFTER\n" +
                "- Avoid driving, except in emergencies.\n" +
                "- Be aware of the risk of electrocution. Do not touch electrical equipment if it is wet or if you are standing in water. If it is safe to do so, turn off the electricity to prevent electric shock.\n" +
                "- Avoid wading in floodwater, which can contain dangerous debris and be contaminated. Underground or downed power lines can also electrically charge the water.\n");

        List<String> hurricanes = new ArrayList<>();
        hurricanes.add("Prepare BEFORE\n" +
                "- If you are at risk for flash flooding, watch for warning signs such as heavy rain.\n" +
                "- Become familiar with your evacuation zone, the evacuation route, and shelter locations.\n" +
                "Gather needed supplies for at least three days. Keep in mind each person’s specific needs, including medication. Don’t forget the needs of pets.\n" +
                "- Keep important documents in a safe place or create password-protected digital copies.\n" +
                "- Protect your property. Declutter drains and gutters. Install check valves in plumbing to prevent backups. Consider hurricane shutters. Review insurance policies.\n" +
                "\n" +
                "Survive DURING\n" +
                "- If told to evacuate, do so immediately. Do not drive around barricades.\n" +
                "- Shelter in a small, interior, windowless room or hallway on the lowest floor that is not subject to flooding.\n" +
                "- If trapped in a building by flooding, go to the highest level of the building. Do not climb into a closed attic. You may become trapped by rising flood water.\n" +
                "- Do not walk, swim, or drive through flood waters. Turn Around. Don’t Drown! Just six inches of fast-moving water can knock you down, and one foot of moving water can sweep your vehicle away.\n" +
                "- Stay off of bridges over fast-moving water.\n" +
                "\n" +
                "Be Safe AFTER\n" +
                "- Do not touch electrical equipment if it is wet or if you are standing in water. If it is safe to do so, turn off electricity at the main breaker or fuse box to prevent electric shock.\n" +
                "- Avoid wading in flood water, which can contain dangerous debris. Underground or downed power lines can also electrically charge the water.\n");


        listHash.put(listDataHeader.get(0), earthquakes);
        listHash.put(listDataHeader.get(1), floods);
        listHash.put(listDataHeader.get(2), hurricanes);
    }
}
package com.example.lenovo.heldenemergencyapp;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class

MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public GoogleApiClient googleApiClient;
    IpService ipService;
    private static final String TAG = "MainActivity";
    public Button logout;
    public TextView currentLocation;
    public Session session;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    public LocationManager locationManager;
    public static String tvLongi;
    public static String tvLati;
    public static String tvLoc;
    public TextView tvLatitude;
    public TextView tvLongitude;
    private Address address;
    public Button btn_send;
    public static ClientData clientData;
    public static String json;
    public JSONObject jsonObject;
    JSONArray jsonArray;
    public String json_string;
    ListView lv;
    ArrayList<EventData>eventsDataa;
    ExpandableListAdapter expandableListAdapter;
    Button danger_button,safe_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();


    }

   public void init()
    {   geoLocateStart();
        eventsDataa=new ArrayList<>();
        lv=findViewById(R.id.lvEvent);
        lv.setAdapter(new EventListAdapter(this.getApplicationContext(),eventsDataa));
      //  jsonStuff();
        clientData=new ClientData();
        session = new Session(this);
        ipService=Common.getIpService();
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();
        LocationPermission locationPermission = new LocationPermission();
        SharedPreferences mSharedPreferences = getSharedPreferences(getResources().getString(R.string.PREFERENCE), Context.MODE_PRIVATE);
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        danger_button=findViewById(R.id.btn_sendDanger);
        safe_button=findViewById(R.id.btn_sendSafe);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigation = findViewById(R.id.navigationView);
        View linearLayout = navigation.inflateHeaderView(R.layout.navigation_header);
        TextView name = linearLayout.findViewById(R.id.profileTextView);
        CircleImageView profileImage = linearLayout.findViewById(R.id.profile_image);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        name.setText(mSharedPreferences.getString(getResources().getString(R.string.PREF_NAME), "User"));
        Toast.makeText(getApplicationContext(), mSharedPreferences.getString(getResources().getString(R.string.PREF_NAME), ""), Toast.LENGTH_LONG).show();
        clientData.setUserName(mSharedPreferences.getString(getResources().getString(R.string.PREF_NAME), ""));
        Glide.with(this)
                .load(mSharedPreferences.getString(getResources().getString(R.string.PREF_URL), ""))
                .apply(new RequestOptions()
                        .placeholder(R.drawable.profile)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(profileImage);

        mEditor.putString(getResources().getString(R.string.PREF_URL), "");
        mEditor.apply();
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.app_home:
                        goHome();
                        return true;

                    case R.id.app_logout:
//                        clearSharePref();
////                        startActivity(new Intent(getApplicationContext(),SplashScreenActivity.class));
                        signOut();
                        return true;

                    case R.id.app_maps:
                        if(isInternetActive())
                            startActivity(new Intent(getApplicationContext(), MapsActivity.class));
                        else
                            toastIt("Please connect to internet");
                        return true;
                    case R.id.app_user_guide:
                        startActivity(new Intent(getApplicationContext(), EmergencyGuideActivity.class));
                }
                return false;
            }
        });
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }


    public void onResume() {
        super.onResume();
        getLocation();
    }

    public void danger_click(View view)
    {
        try{
            //  convertor();
            Log.d("nah", convertor());
            MessageSender messageSender=new MessageSender();
            messageSender.execute("danger");
            toastIt("Data sent");
        }
        catch (Exception e)
        {
            Log.d("ups", "bad thing");
        }


    }

    public void safe_click(View view)
    {
        try{
            //  convertor();
            Log.d("nah", convertor());
            MessageSender messageSender=new MessageSender();
            messageSender.execute("safe");
            toastIt("Data sent");
        }
        catch (Exception e)
        {
            Log.d("ups", "bad thing");
        }


    }

    public void jsonStuff()
    {json_string=convertor();
        try {
            JSONArray jsonArray= new JSONArray(json_string);
            JSONObject jo;
            eventsDataa.clear();
            EventData eventData;

            for(int i=0;i<jsonArray.length();i++)
            {
                jo=jsonArray.getJSONObject(i);

               // long event_id=jo.getLong("event_id");
                String event_name=jo.getString("userName");
                String event_description=jo.getString("location");

                eventData=new EventData();

              //  eventData.setEventId(event_id);
                eventData.setEventName(event_name);
                eventData.setEventDescription(event_description);

                eventsDataa.add(eventData);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
//        try {
//            jsonObject=new JSONObject(json_string);
//            jsonArray=jsonObject.getJSONArray(json_string);
//            int count=0;
//            String name,description;
//            long id;
//            while(count<jsonObject.length())
//            {
//                JSONObject JO=jsonArray.getJSONObject(count);
//                id=JO.getLong("id");
//                name=JO.getString("name");
//                description=JO.getString("description");
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

//    public void send()
//    {
//        btn_send=findViewById(R.id.btn_send);
//        btn_send.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getIpAddress();
//               // Log.d(TAG, clientData.getLocation());
//            }
//        });
//
//    }

    private void getIpAddress()
    {
        ipService.getIP().enqueue(new Callback<IP>() {
            @Override
            public void onResponse(Call<IP> call, Response<IP> response) {
                Log.d("GETIP",response.body().getIp());

            }

            @Override
            public void onFailure(Call<IP> call, Throwable t) {
                Log.d(TAG,"Error");
            }
        });
    }


    private boolean isInternetActive()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).
                getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).
                        getState() == NetworkInfo.State.CONNECTED;
    }


    private void geoLocateStart() {
        Geocoder geocoder = new Geocoder(MainActivity.this);
        try {
            geocoder.getFromLocationName("New York", 1);
        } catch (IOException ignored) { }
    }

            private void goHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // Getting reference to TextView tv_longitude
        tvLongitude = findViewById(R.id.tv_longitude);
        // Getting reference to TextView tv_latitude
        tvLatitude = findViewById(R.id.tv_latitude);
        currentLocation = findViewById(R.id.current_location);


        tvLongi = String.valueOf(location.getLongitude());
        clientData.setLongitude(String.valueOf(location.getLongitude()));
        tvLati = String.valueOf(location.getLatitude());
        clientData.setLatitude(String.valueOf(location.getLatitude()));
        tvLoc=location.toString();

        // Setting Current Longitude
        tvLongitude.setText(tvLongi);
        // Setting Current Latitude
        tvLatitude.setText(tvLati);
        geoLocateCoordinates();
        currentLocation.setText(address.getLocality());
        clientData.setLocation(address.getLocality());



        try{
          //  convertor();
            Log.d("Haaah", convertor());

            MessageSender messageSender=new MessageSender();
            messageSender.execute("location");
          //  Toast.makeText(getApplicationContext(),"Data sent", Toast.LENGTH_LONG).show();
        }
        catch (Exception e)
        {
            Log.d("ups", "bad thing");
        }
    }


    private void geoLocateCoordinates() {
        Geocoder geocoder = new Geocoder(MainActivity.this);
        List<Address> list = new ArrayList<>();
            try {
                list = geocoder.getFromLocation(Double.parseDouble(tvLati), Double.parseDouble(tvLongi), 1);
            } catch (IOException e) {
                Log.e(TAG, "geoLocate: IOException: " + e.getMessage());
            }
            if (list.size() > 0) {
                address = list.get(0);
                if (address.getLocality() == null) {
                    toastIt("That's not a locality");
                } else {
                    Log.d(TAG, "geoCoordinates: found a location: " + address.toString());
                    currentLocation.setText(address.getLocality());
                }
            }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(MainActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider!" + provider,
                Toast.LENGTH_SHORT).show();
    }


    private void toastIt(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, -140);
        toast.show();
    }

    @SuppressLint("ShowToast")
    public void signOut() {
 //       session.setLoggedin(false);

 //       if (Session.loggedInGoogle) {
//            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
//                @Override
//                public void onResult(@NonNull Status status) {
//
//                }
//            });
//
//            Intent intent = new Intent(this, LoginActivity.class);
//            toastIt("From GOOGLE " + Session.loggedInGoogle);
//
//            startActivity(intent);
//            try {
//                finish();
//            } catch (Exception e) {
//                toastIt("Erorr : ");
//            }
//        } else {
//
//
//            Intent intent = new Intent(this, LoginActivity.class);
//            toastIt("From GOOGLE ");
//
//            startActivity(intent);
//        }
//        Session.loggedInGoogle = false;
//        finish();

            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // ...
//                            Toast.makeText(getApplicationContext(),"Logged Out",Toast.LENGTH_SHORT).show();
//                            Intent i=new Intent(getApplicationContext(),MainActivity.class);
//                            startActivity(i);
                                                   clearSharePref();
                        startActivity(new Intent(getApplicationContext(),LoginActivity.class));

                        }
                    });
        }


    public static String convertor()
    {
        Gson gson = new Gson();
        json = gson.toJson(clientData);

        return json;
    }

    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        googleApiClient.connect();
        super.onStart();
    }

    private void clearSharePref(){
        SharedPreferences mSharedPreferences = getSharedPreferences(getResources().getString(R.string.PREFERENCE), Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putString(getResources().getString(R.string.PREF_NAME),null);
        mEditor.putString(getResources().getString(R.string.PREF_URL),null);
        try{

            finish();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"Erorr : " ,Toast.LENGTH_SHORT);
        }
        mEditor.apply();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        toastIt("Connection Failed");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
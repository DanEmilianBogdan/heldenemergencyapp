﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// data trasnfer objects 
namespace DTOs
{
    [Serializable]
    public class Event
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string County { get; set; }

        public Event() { }

        public Event(int id)
        {
            this.id = id;
        }

        public Event(string Name,string Description,int Status, string County)
        {
            this.Name = Name;
            this.Description = Description;
            this.Status = Status;
            this.County = County;
        }


        public Event(int id,string Name, string Description, int Status)
        {
            this.Name = Name;
            this.Description = Description;
            this.Status = Status;
            this.id = id;
        }
    }
}

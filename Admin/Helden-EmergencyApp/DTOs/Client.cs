﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// data trasnfer objects 
namespace DTOs
{
    [Serializable]
    public class Client
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int Status { get; set; }
        public string Location { get; set; }

        public Client() { }

        public Client(int id, string Name, string Latitude, string Longitude, int Status, string Location)
        {
            this.id = id;
            this.Name = Name;
            this.Longitude = Longitude;
            this.Latitude = Latitude;
            this.Status = Status;
            this.Location = Location;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace Helden_EmergencyApp
{
    public partial class AddEvent : Form
    {
        // Round Corners
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );
        private const int WM_NCHITTEST = 0x84;
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;
        // Add Shadows around the main form
        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
        public AddEvent()
        {
            InitializeComponent();
            // Above code is essential for a correct redraw of the ClientRectangle after a minimize
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            // Initialize Round Corners
            this.FormBorderStyle = FormBorderStyle.None;
            Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, this.Width, this.Height, 20, 20));
        }

        // Move Form
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_NCHITTEST)
            { m.Result = (IntPtr)(HT_CAPTION); }
            return;
        }
        // Gradient Background
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (ClientRectangle.Width == 0 || ClientRectangle.Height == 0)
            { return; }
            else
            {
                string firstHex = "#1A2A6C";    // Blue
                string secondHex = "#B21F1F";   // Red
                Color firstColor = ColorTranslator.FromHtml(firstHex);
                Color secondColor = ColorTranslator.FromHtml(secondHex);
                System.Drawing.Drawing2D.LinearGradientBrush myBrush = new LinearGradientBrush(this.ClientRectangle, firstColor, secondColor, 70F);
                // base.OnPaintBackground(e);
                e.Graphics.FillRectangle(myBrush, this.ClientRectangle);
            }
            return;
        }

        private void addEventButton_Click(object sender, EventArgs e)
        {
            Application.OpenForms["Form1"].Controls["nameTxt"].Text = nameTxt.Text;
            Application.OpenForms["Form1"].Controls["descriptionTxt"].Text = descriptionTxt.Text;
            if(statusComboBox.Items[statusComboBox.SelectedIndex].ToString() == "ON")
            {
                Application.OpenForms["Form1"].Controls["statusTxt"].Text = "1" ;

            }
            if(statusComboBox.Items[statusComboBox.SelectedIndex].ToString() == "OFF")
            {
                Application.OpenForms["Form1"].Controls["statusTxt"].Text = "0" ;
            }
            Application.OpenForms["Form1"].Controls["countyTxt"].Text = countyTxt.Text;
            this.Close();
        }

        private void AddEvent_Load(object sender, EventArgs e)
        {
            GMapProviders.GoogleMap.ApiKey = @"AIzaSyBQNn9geyjuMmPC_VRLvXtCPfvVF5rsYbU";
            map.ShowCenter = false;
            map.MapProvider = GMapProviders.GoogleMap;
            map.Position = new PointLatLng(46, 25);
            map.MinZoom = 6;
            map.MaxZoom = 100;
            map.Zoom = 6;
        }
            
        private List<String> GetAddress(PointLatLng point)
        {
            List<Placemark> placemarks = null;
            var statusCode = GMapProviders.GoogleMap.GetPlacemarks(point, out placemarks);
            if(statusCode==GeoCoderStatusCode.G_GEO_SUCCESS && placemarks != null)
            {
                List<String> addresses = new List<string>();
                foreach(var placemark in placemarks)
                {
                    addresses.Add(placemark.AdministrativeAreaName);
                }
                return addresses;

            }return null;
        }

        private void map_MouseClick(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                var point = map.FromLocalToLatLng(e.X, e.Y);
                double latitude = point.Lat;    
                double longitude = point.Lng;

                latitudeTxt.Text = latitude + "";
                longitudeTxt.Text = longitude + "";

                map.Position = point;

                var markers = new GMapOverlay("markers");
                var marker = new GMarkerGoogle(point, GMarkerGoogleType.red_pushpin);

                markers.Markers.Add(marker);
                map.Overlays.Add(markers);

                string[] counties = new string[] { "Călărași","Caraș-Severin","Brăila", "Alba","Suceava", "Neamț", "Botoșani", "Iași", "Bistrița-Năsăud" , "București", "Maramureș", "Prahova", "Cluj", "Timiș", "Dolj", "Bacău", "Argeș", "Bihor", "Mureș", "Galați", "Brașov", "Dâmbovița", "Maramureș", "Buzău", "Olt", "Arad", "Hunedoara","Ialomița", "Ilfov", "Mehedinți", "Prahova", "Satu Mare", "Sălaj", "Sibiu", "Teleorman", "Tulcea", "Vaslui", "Vâlcea", "Vrancea" };
                var address = GetAddress(point);
                string[] markAddress = address.ToArray();

                foreach (string county in counties)
                {
                    if (markAddress[0].IndexOf(county) != -1)
                    {
                        if(countyTxt.Text.Equals("")) 
                        countyTxt.Text = countyTxt.Text + county;
                        else
                        countyTxt.Text = countyTxt.Text + " , " + county;
                    }
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            return;
        }
    }
}

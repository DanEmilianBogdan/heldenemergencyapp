﻿namespace Helden_EmergencyApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.clientsDGV = new System.Windows.Forms.DataGridView();
            this.createEventButton = new System.Windows.Forms.Button();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.statusTxt = new System.Windows.Forms.TextBox();
            this.descriptionTxt = new System.Windows.Forms.TextBox();
            this.countyTxt = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.map = new GMap.NET.WindowsForms.GMapControl();
            this.loadIntoMapButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.timerTxt = new System.Windows.Forms.TextBox();
            this.eventsDGV = new System.Windows.Forms.DataGridView();
            this.timerETxt = new System.Windows.Forms.TextBox();
            this.makeEventInactiveButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.updateEvents = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.clientsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // clientsDGV
            // 
            this.clientsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientsDGV.Location = new System.Drawing.Point(12, 64);
            this.clientsDGV.Name = "clientsDGV";
            this.clientsDGV.RowTemplate.Height = 24;
            this.clientsDGV.Size = new System.Drawing.Size(546, 180);
            this.clientsDGV.TabIndex = 1;
            // 
            // createEventButton
            // 
            this.createEventButton.Location = new System.Drawing.Point(49, 256);
            this.createEventButton.Name = "createEventButton";
            this.createEventButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.createEventButton.Size = new System.Drawing.Size(124, 48);
            this.createEventButton.TabIndex = 3;
            this.createEventButton.Text = "Create event";
            this.createEventButton.UseVisualStyleBackColor = true;
            this.createEventButton.Click += new System.EventHandler(this.createEventButton_Click);
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(171, 316);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(76, 22);
            this.nameTxt.TabIndex = 4;
            this.nameTxt.Visible = false;
            // 
            // statusTxt
            // 
            this.statusTxt.Location = new System.Drawing.Point(171, 344);
            this.statusTxt.Name = "statusTxt";
            this.statusTxt.Size = new System.Drawing.Size(76, 22);
            this.statusTxt.TabIndex = 5;
            this.statusTxt.Visible = false;
            // 
            // descriptionTxt
            // 
            this.descriptionTxt.Location = new System.Drawing.Point(253, 316);
            this.descriptionTxt.Name = "descriptionTxt";
            this.descriptionTxt.Size = new System.Drawing.Size(76, 22);
            this.descriptionTxt.TabIndex = 6;
            this.descriptionTxt.Visible = false;
            // 
            // countyTxt
            // 
            this.countyTxt.Location = new System.Drawing.Point(253, 344);
            this.countyTxt.Name = "countyTxt";
            this.countyTxt.Size = new System.Drawing.Size(76, 22);
            this.countyTxt.TabIndex = 9;
            this.countyTxt.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // map
            // 
            this.map.Bearing = 0F;
            this.map.CanDragMap = true;
            this.map.EmptyTileColor = System.Drawing.Color.Navy;
            this.map.GrayScaleMode = false;
            this.map.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.map.LevelsKeepInMemmory = 5;
            this.map.Location = new System.Drawing.Point(597, 64);
            this.map.MarkersEnabled = true;
            this.map.MaxZoom = 2;
            this.map.MinZoom = 2;
            this.map.MouseWheelZoomEnabled = true;
            this.map.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.map.Name = "map";
            this.map.NegativeMode = false;
            this.map.PolygonsEnabled = true;
            this.map.RetryLoadTile = 0;
            this.map.RoutesEnabled = true;
            this.map.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.map.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.map.ShowTileGridLines = false;
            this.map.Size = new System.Drawing.Size(754, 449);
            this.map.TabIndex = 10;
            this.map.Zoom = 0D;
            // 
            // loadIntoMapButton
            // 
            this.loadIntoMapButton.Location = new System.Drawing.Point(232, 258);
            this.loadIntoMapButton.Name = "loadIntoMapButton";
            this.loadIntoMapButton.Size = new System.Drawing.Size(124, 46);
            this.loadIntoMapButton.TabIndex = 13;
            this.loadIntoMapButton.Text = "Show clients location";
            this.loadIntoMapButton.UseVisualStyleBackColor = true;
            this.loadIntoMapButton.Click += new System.EventHandler(this.loadIntoMapButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(464, 475);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Next update in :";
            // 
            // timerTxt
            // 
            this.timerTxt.Location = new System.Drawing.Point(569, 465);
            this.timerTxt.Name = "timerTxt";
            this.timerTxt.Size = new System.Drawing.Size(22, 22);
            this.timerTxt.TabIndex = 16;
            // 
            // eventsDGV
            // 
            this.eventsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.eventsDGV.Location = new System.Drawing.Point(12, 377);
            this.eventsDGV.Name = "eventsDGV";
            this.eventsDGV.RowTemplate.Height = 24;
            this.eventsDGV.Size = new System.Drawing.Size(449, 136);
            this.eventsDGV.TabIndex = 20;
            this.eventsDGV.SelectionChanged += new System.EventHandler(this.eventsDGV_SelectionChanged);
            // 
            // timerETxt
            // 
            this.timerETxt.Location = new System.Drawing.Point(569, 491);
            this.timerETxt.Name = "timerETxt";
            this.timerETxt.Size = new System.Drawing.Size(22, 22);
            this.timerETxt.TabIndex = 21;
            // 
            // makeEventInactiveButton
            // 
            this.makeEventInactiveButton.Location = new System.Drawing.Point(467, 377);
            this.makeEventInactiveButton.Name = "makeEventInactiveButton";
            this.makeEventInactiveButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.makeEventInactiveButton.Size = new System.Drawing.Size(124, 48);
            this.makeEventInactiveButton.TabIndex = 22;
            this.makeEventInactiveButton.Text = "Close event";
            this.makeEventInactiveButton.UseVisualStyleBackColor = true;
            this.makeEventInactiveButton.Click += new System.EventHandler(this.makeEventInactiveButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(31, 31);
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(49, 15);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 25);
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // updateEvents
            // 
            this.updateEvents.Location = new System.Drawing.Point(407, 256);
            this.updateEvents.Name = "updateEvents";
            this.updateEvents.Size = new System.Drawing.Size(124, 46);
            this.updateEvents.TabIndex = 26;
            this.updateEvents.Text = "Update events";
            this.updateEvents.UseVisualStyleBackColor = true;
            this.updateEvents.Click += new System.EventHandler(this.updateEvents_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(825, 8);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(525, 50);
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(1363, 524);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.updateEvents);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.makeEventInactiveButton);
            this.Controls.Add(this.timerETxt);
            this.Controls.Add(this.eventsDGV);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.timerTxt);
            this.Controls.Add(this.loadIntoMapButton);
            this.Controls.Add(this.map);
            this.Controls.Add(this.countyTxt);
            this.Controls.Add(this.descriptionTxt);
            this.Controls.Add(this.statusTxt);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.createEventButton);
            this.Controls.Add(this.clientsDGV);
            this.Name = "Form1";
            this.Text = "v";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.clientsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView clientsDGV;
        private System.Windows.Forms.Button createEventButton;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.TextBox statusTxt;
        private System.Windows.Forms.TextBox descriptionTxt;
        private System.Windows.Forms.TextBox countyTxt;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private GMap.NET.WindowsForms.GMapControl map;
        private System.Windows.Forms.Button loadIntoMapButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox timerTxt;
        private System.Windows.Forms.DataGridView eventsDGV;
        private System.Windows.Forms.TextBox timerETxt;
        private System.Windows.Forms.Button makeEventInactiveButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button updateEvents;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}


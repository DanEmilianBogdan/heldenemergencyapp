﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using DTOs;
using Newtonsoft.Json;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using Newtonsoft.Json.Linq;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;

namespace Helden_EmergencyApp
{
    public partial class Form1 : Form
    {
        // Round Corners
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );
        private const int WM_NCHITTEST = 0x84;
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;
        // Add Shadows around the main form
        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
        string serverIP = "192.168.43.60";
        int port = 9090;
        Socket listenerSocket;
        Timer timer = new Timer();
        Timer timerE = new Timer();
        int count_down =20;
        int count_downE =25;
        int idToModify=-1;
        public static string s1 = "", s2 = "", s3 = "", s4 = "";

        public Form1()
        {
            InitializeComponent();
            // Above code is essential for a correct redraw of the ClientRectangle after a minimize
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            // Initialize Round Corners
            this.FormBorderStyle = FormBorderStyle.None;
            Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, this.Width, this.Height, 20, 20));
        }

        // Move Form
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_NCHITTEST)
            { m.Result = (IntPtr)(HT_CAPTION); }
            return;
        }
        // Gradient Background
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (ClientRectangle.Width == 0 || ClientRectangle.Height == 0)
            { return; }
            else
            {
                string firstHex = "#1A2A6C";    // Blue
                string secondHex = "#B21F1F";   // Red
                Color firstColor = ColorTranslator.FromHtml(firstHex);
                Color secondColor = ColorTranslator.FromHtml(secondHex);
                System.Drawing.Drawing2D.LinearGradientBrush myBrush = new LinearGradientBrush(this.ClientRectangle, firstColor, secondColor, 70F);
                // base.OnPaintBackground(e);
                e.Graphics.FillRectangle(myBrush, this.ClientRectangle);
            }
            return;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Initialize_Timer();
            Initialize_TimerE();
            GMapProviders.GoogleMap.ApiKey = @"AIzaSyBQNn9geyjuMmPC_VRLvXtCPfvVF5rsYbU";
            map.ShowCenter = false;
            map.MapProvider = GMapProviders.GoogleMap;
            map.Position = new PointLatLng(46, 25);
            map.MinZoom = 6;
            map.MaxZoom = 100;
            map.Zoom = 6;
            getClients();
            getEvents();
        }

        private void loadIntoMapButton_Click(object sender, EventArgs e)
        {
            map.ShowCenter = false;
            map.MapProvider = GMapProviders.GoogleMap;
            map.Position = new PointLatLng(46, 25);
            map.MinZoom = 6;
            map.MaxZoom = 100;
            map.Zoom = 6;

            double[] markerCoordinatesLong = new double[clientsDGV.RowCount];
            double[] markerCoordinatesLat = new double[clientsDGV.RowCount];
            int[] markerStatus = new int[clientsDGV.RowCount];

            int iCol = 2;
            int rowCount = clientsDGV.RowCount;

            for (int iRow = 0; iRow < rowCount; iRow++)
            {
                markerCoordinatesLong[iRow]= Convert.ToDouble( clientsDGV[iCol, iRow].Value);
            }
            iCol = 3;
            for (int iRow = 0; iRow < rowCount; iRow++)
            {
                markerCoordinatesLat[iRow] = Convert.ToDouble(clientsDGV[iCol, iRow].Value);
            }
            iCol = 4;
            for (int iRow = 0; iRow < rowCount; iRow++)
            {
               markerStatus[iRow] = Convert.ToInt32(clientsDGV[iCol, iRow].Value);
            }

            iCol = 0;
            foreach (double i in markerCoordinatesLong)
            {
                PointLatLng point = new PointLatLng(i, markerCoordinatesLat[iCol]);
                iCol++;
                double latitude = point.Lat;
                double longitude = point.Lng;

                map.Position = point;

                
                //if (markerStatus[iCol] == 0)
                //{
                    var markers = new GMapOverlay("markers");
                    var marker = new GMarkerGoogle(point, GMarkerGoogleType.red_pushpin);
                //}

                markers.Markers.Add(marker);
                map.Overlays.Add(markers);
            }
        }

        private void createEventButton_Click(object sender, EventArgs e)
        {
            AddEvent addEvent = new AddEvent();
            addEvent.Show();
        }

        private void Initialize_Timer()
        {
            timer.Interval = 1000;
            timer.Tick += new EventHandler(TimerTick);
            timer.Start();
        }
        private void Initialize_TimerE()
        {
            timerE.Interval = 1000;
            timerE.Tick += new EventHandler(TimerTickE);
            timerE.Start();
        }

        private void eventsDGV_SelectionChanged(object sender, EventArgs e)
        {
            int rowindex = eventsDGV.CurrentCell.RowIndex;
            int columnindex = eventsDGV.CurrentCell.ColumnIndex;

            idToModify = Convert.ToInt32( eventsDGV.Rows[rowindex].Cells[columnindex].Value);
            
        }

        private void TimerTick(object sender, EventArgs e)
        {
            count_down--;
            timerTxt.Text = count_down.ToString();
            if (count_down == 0)
            {
                count_down = 20;
                timer.Stop();
                timer.Start();
                getClients();
            }
        }

        private void TimerTickE(object sender, EventArgs e)
        {
            count_downE--;
            timerETxt.Text = count_downE.ToString();
            if (count_downE == 0)
            {
                count_downE = 25;
                timerE.Stop();
                timerE.Start();
                getEvents();
            }
        }

        private void makeEventInactiveButton_Click(object sender, EventArgs e)
        {
            modifyEvent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            return;
        }

        private void modifyEvent()
        {
            if(idToModify!=-1)
            try
            {
                listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(serverIP), port);
                listenerSocket.Connect(endPoint);

                string s = "edit";
                byte[] byData = System.Text.Encoding.UTF8.GetBytes(s);
                byte[] byDataRec = new byte[1000];

                listenerSocket.Send(byData);
                listenerSocket.Receive(byDataRec);
                Event event1 = new Event(idToModify);
                string jsonFile = JsonConvert.SerializeObject(event1);

                if (Encoding.Default.GetString(byDataRec).Contains("edit"))
                {
                    byData = Encoding.UTF8.GetBytes(jsonFile);
                    listenerSocket.Send(byData);
                }
                listenerSocket.Close();
                idToModify = -1;
                eventsDGV.CurrentCell.Selected = false;

            }
            catch (System.Net.Sockets.SocketException e)
            {
                MessageBox.Show("Connection to server failed.", "Server connection error");
            }
            else
                MessageBox.Show("Please select an event.", "Information missing.");
        }

        private void updateEvents_Click(object sender, EventArgs e)
        {
            if(statusTxt.Text!="")
                sendEvent();
        }

        private void getClients()
        {
            try { 
            listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(serverIP), port);
            listenerSocket.Connect(endPoint);

            string s = "clienti";
            byte[] byData = System.Text.Encoding.UTF8.GetBytes(s);
            byte[] byDataRec = new byte[1000];

            listenerSocket.Send(byData);
            listenerSocket.Receive(byDataRec);
            listenerSocket.Close();
            string clienti = Encoding.Default.GetString(byDataRec);
            var result = JsonConvert.DeserializeObject<List<Client>>(clienti);
            clientsDGV.DataSource = result.Select(c => new { c.id, c.Name, c.Latitude, c.Longitude, c.Status }).ToList();
            }
            catch (System.Net.Sockets.SocketException e)
            {
                MessageBox.Show("Connection to server failed.", "Server connection error");
            }
            catch(System.Exception e)
            {
                MessageBox.Show("Another");
            }
        }

        private void getEvents()
        {
            try
            {
                listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(serverIP), port);
                listenerSocket.Connect(endPoint);

                string s = "events";
                byte[] byData = System.Text.Encoding.UTF8.GetBytes(s);
                byte[] byDataRec = new byte[1000];

                listenerSocket.Send(byData);
                listenerSocket.Receive(byDataRec);
                listenerSocket.Close();
                string evenimente = Encoding.Default.GetString(byDataRec);
                var result = JsonConvert.DeserializeObject<List<Event>>(evenimente);

                eventsDGV.DataSource = result.Select(c => new { c.id, c.Name,c.Description, c.Status }).ToList();
                eventsDGV.CurrentCell.Selected = false;
            }
            catch(System.Net.Sockets.SocketException e)
            {
                MessageBox.Show("Connection to server failed.", "Server connection error");
            }
        }

        private void sendEvent()
        {
            listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(serverIP), port);
            listenerSocket.Connect(endPoint);

            string s = "adauga";
            byte[] byData = System.Text.Encoding.UTF8.GetBytes(s);
            byte[] byDataRec = new byte[1000];

            listenerSocket.Send(byData);
            listenerSocket.Receive(byDataRec);

            s1 = nameTxt.Text; s2 = descriptionTxt.Text; s3 = statusTxt.Text; s4 = countyTxt.Text;
            string temp = s4.Replace("ș", "s");
            temp = temp.Replace("ă", "a");
            temp = temp.Replace("ț", "t");
            s4 = temp;

            Event event1 = new Event(s1, s2, Int32.Parse(s3), s4);
            string jsonFile = JsonConvert.SerializeObject(event1);

            if (Encoding.Default.GetString(byDataRec).Contains("send"))
            {
                byData = Encoding.UTF8.GetBytes(jsonFile);
                listenerSocket.Send(byData);
            }
            listenerSocket.Close();
            statusTxt.Text = "";
        }
    }
}


